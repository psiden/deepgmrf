%% %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% PURPOSE:      Compute true posterior of the
%               example simulations of GMRF CAR(2)
%
% FIRST VER.:   2019-10-17
% REVISED:
%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% Simulate parameters from spatial priors
clear all
close all
clc
gmrf_nbr = 1;
do_save = 0;

load(strcat('data/indata/gmrf',num2str(gmrf_nbr),'.mat'),'GMRFsample')
y = GMRFsample(:);

sz_pad = [180,140];
sz = [160,120];
tau2 = 1;
range = 50;
kappa2 = 8/(range^2); % 0; %
alpha = 2;
use_ichol = 0;

N = prod(sz_pad);
bmask = 1:N;
ndim = length(sz_pad);

if alpha == 1
  QTypes = {'L'};
  [QList,GQList] = setupPrecMats(QTypes,N,sz_pad,bmask,ndim);
  GQ = GQList{1};
  Q = tau2*(kappa2*speye(N) + GQ'*GQ);
  %   NGQ = size(GQ,1);
  %   b = sqrt(tau2) * (sqrt(kappa2) * randn(N,1) + GQ' * randn(NGQ,1));
elseif alpha == 2
  QTypes = {'L'};
  [QList,GQList] = setupPrecMats(QTypes,N,sz_pad,bmask,ndim);
  GQ = sqrt(tau2) * (QList{1} + kappa2*speye(N));
  %   Q = GQ'*GQ;
  %   NGQ = size(GQ,1);
  %   b = GQ' * randn(NGQ,1);
  Q = GQ'*GQ; % Works as well
  %   b = randn(N,1);
end

%% Mask

mask = ones(sz);
mask(.4*sz(1)+1:.6*sz(1),.3*sz(2)+1:.7*sz(2)) = 0;
mask(.7*sz(1)+1:.9*sz(1),.3*sz(2)+1:.3*sz(2)+8) = 0;
mask(.7*sz(1)+1:.9*sz(1),.5*sz(2)+1:.5*sz(2)+4) = 0;
mask(.7*sz(1)+1:.9*sz(1),.7*sz(2):.7*sz(2)) = 0;
mask(.1*sz(1)+1:.1*sz(1)+8,.3*sz(2)+1:.7*sz(2)) = 0;
mask(.2*sz(1)+1:.2*sz(1)+4,.3*sz(2)+1:.7*sz(2)) = 0;
mask(.3*sz(1)+1:.3*sz(1)+1,.3*sz(2)+1:.7*sz(2)) = 0;
% mask(65:96,37:84) = 0;
mask_pad = zeros(sz_pad);
mask_pad(11:sz(1)+10,11:sz(2)+10) = mask;
mask_padI = find(mask_pad(:));


%% Infer mean

lambda = 1/0.000001;
QTilde = Q + lambda*spdiags(mask_pad(:),0,N,N);
ytilde = zeros(N,1);
ytilde(mask_padI) = y(find(mask(:)));
b = lambda*ytilde;

% QI = amd(QTilde);
% QP = QTilde(QI,QI);
% bP = b(QI);
if use_ichol
  %   M1 = ichol(QP);
  %   [muP,flag,relres] = pcg(QP,bP,1e-9,1000,M1,M1',zeros(N,1));
else
  [mu,flag,relres] = pcg(QTilde,b,1e-9,1000);
end
% muTemp = zeros(N,1);
% muTemp(QI) = muP;
% mu = muTemp';

disp(strcat("PCG error, mean: ",num2str(relres)))
mu_im = reshape(mu,sz_pad);
mu_im = mu_im(floor(sz_pad(1)/2-sz(1)/2)+1:floor(sz_pad(1)/2-sz(1)/2)+sz(1),...
  floor(sz_pad(2)/2-sz(2)/2)+1:floor(sz_pad(2)/2-sz(2)/2)+sz(2));

figure(2);
imagesc(mu_im);colorbar;axis image;
% title(strcat("CAR(",num2str(alpha),") with range ",num2str(sqrt(8/kappa2))));

if do_save
  save(strcat('../../data/indata/postmean',num2str(gmrf_nbr),'.mat'),'mu_im')
  % % imwrite(GMRFsample,'GMRFsample.png')
end

%% Sample from posterior

Ns = 100; % 20; %
bs = GQ'*randn(N,Ns) + lambda*(repmat(ytilde,[1,Ns])+1/sqrt(lambda)*mask_pad(:).*randn(N,Ns));
% bs = GQ'*zeros(N,Ns) + lambda*(repmat(ytilde,[1,Ns])+1/sqrt(lambda)*mask_pad(:).*zeros(N,Ns));
% bsP = bs(QI,:);
xs = zeros(N,Ns);

for i = 1:Ns
  [xsTemp,flag,relres] = pcg(QTilde,bs(:,i),1e-9,1000);
  disp(strcat("PCG error, posterior sample ",num2str(i),": ",num2str(relres)))
  xs(:,i) = xsTemp;
end

xs_im = reshape(xs,[sz_pad,Ns]);
xs_im = xs_im(floor(sz_pad(1)/2-sz(1)/2)+1:floor(sz_pad(1)/2-sz(1)/2)+sz(1),...
  floor(sz_pad(2)/2-sz(2)/2)+1:floor(sz_pad(2)/2-sz(2)/2)+sz(2),:);
figure(3);
imagesc(xs_im(:,:,1));colorbar;axis image;
if do_save
  save(strcat('../../data/indata/postsamples',num2str(gmrf_nbr),'.mat'),'xs_im')
end


%% Alternative CG sampling
% global A;
% A = QTilde;
% 
% xs2 = zeros(N,Ns);
% for i = 1:Ns
%   [xs2Temp,relres2,resvec2] = cg(@MMFun,bs(:,i),1e-9,1000);
%   disp(strcat("CG error, posterior sample ",num2str(i),": ",num2str(relres2)))
%   xs2(:,i) = xs2Temp;
% end
% xs2_im = reshape(xs2,[sz_pad,Ns]);
% xs2_im = xs2_im(floor(sz_pad(1)/2-sz(1)/2)+1:floor(sz_pad(1)/2-sz(1)/2)+sz(1),...
%   floor(sz_pad(2)/2-sz(2)/2)+1:floor(sz_pad(2)/2-sz(2)/2)+sz(2),:);
% figure(5);
% imagesc(xs2_im);colorbar;axis image;

%% Approximate marginal sd

diagQ = diag(QTilde);
% margvarRBMC = var(xs,0,2);
margvarRBMC = 1./diag(QTilde) + 1/Ns*sum(((QTilde-diag(diagQ))*(xs-mu) ./ diagQ).^2,2);
sd_im = sqrt(reshape(margvarRBMC,sz_pad));
sd_im = sd_im(floor(sz_pad(1)/2-sz(1)/2)+1:floor(sz_pad(1)/2-sz(1)/2)+sz(1),...
  floor(sz_pad(2)/2-sz(2)/2)+1:floor(sz_pad(2)/2-sz(2)/2)+sz(2));

figure(4);
imagesc(sd_im);colorbar;axis image;
if do_save
  save(strcat('../../data/indata/postsd',num2str(gmrf_nbr),'.mat'),'sd_im')
end

%% Functions
function z = MMFun(x)
  global A;
  z = A*x;
end

function [x,relres,resvec] = cg(Afun,b,tol,maxit)
  N = length(b);
  n2b = norm(b);
  x = zeros(N,1);

  tolb = tol * n2b;                  % Relative tolerance
  r = b - Afun(x);
  normr = norm(r);                   % Norm of residual
  normr_act = normr;

  if (normr <= tolb)                 % Initial guess is a good enough solution
    relres = normr / n2b;
    resvec = normr;
    return
  end

  resvec = zeros(maxit+1,1);         % Preallocate vector for norm of residuals
  resvec(1,:) = normr;               % resvec(1) = norm(b-A*x0)
  normrmin = normr;                  % Norm of minimum residual
  rho = 1;
  beta = 0;
  p = 0;
  
  % loop over maxit iterations (unless convergence or failure)
  for ii = 1:maxit

    z = r;
    rho1 = rho;
    p1 = p;
    rho = r' * z;
    if (ii == 1)
      p = z;
    else
      beta = rho / rho1;
      p = z + beta * p1;
    end
    q = Afun(p);
    pq = p'*q;
    alpha = rho / pq;

    x = x + alpha * p;             % form new iterate
    r = r - alpha * q;
    normr = norm(r);
    normr_act = normr;
    resvec(ii+1,1) = normr;
    
    % check for convergence
    if (normr <= tolb)
      r = b - Afun(x);
      normr_act = norm(r);
      resvec(ii+1,1) = normr_act;
      if (normr_act <= tolb)
        break
      end
    end

    if (normr_act < normrmin)      % update minimal norm quantities
      normrmin = normr_act;
    end
    
%     if mod(ii,100) == 0
%       disp(strcat(num2str(ii),": ",num2str(normr_act / n2b)))
%     end
  end                                % for ii = 1 : maxit

  % returned solution is first with minimal residual
  relres = normr_act / n2b;

  % truncate the zeros from resvec
  resvec = resvec(1:ii+1,:);
end