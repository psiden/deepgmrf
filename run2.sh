echo "Running python code..."

LOGDIR="logs/log"

for i in {11..15}; do
    dataset=52 
#    METHODNAME="Seq-Run1-Seed$i"
#    echo "Running image $dataset with method: $METHODNAME"
#    python main_script.py --dataset_nbr=$dataset --do_plot=0 --num_steps=100000 --learning_rate=0.01 --depth=1  \
#    --non_linear=0 --layer_bias=1 --detrend=1 --learn_sigmasq=0 --use_constr_filter=0 --use_seq_filter=1 \
#    --seq_filter_size=5 --debug=0 --method_name=$METHODNAME --logdir=$LOGDIR-$dataset/$METHODNAME --seed=$i 

#    METHODNAME="Seq-Run3-Seed$i"
#    echo "Running image $dataset with method: $METHODNAME"
#    python main_script.py --dataset_nbr=$dataset --do_plot=0 --num_steps=100000 --learning_rate=0.01 --depth=3  \
#    --non_linear=0 --layer_bias=1 --detrend=1 --learn_sigmasq=0 --use_constr_filter=0 --use_seq_filter=1 \
#    --seq_filter_size=5 --debug=0 --method_name=$METHODNAME --logdir=$LOGDIR-$dataset/$METHODNAME --seed=$i

    METHODNAME="Seq-Run5-Seed$i"
    echo "Running image $dataset with method: $METHODNAME"
    python main_script.py --dataset_nbr=$dataset --do_plot=0 --num_steps=100000 --learning_rate=0.01 --depth=5  \
    --non_linear=0 --layer_bias=1 --detrend=1 --learn_sigmasq=0 --use_constr_filter=0 --use_seq_filter=1 \
    --seq_filter_size=5 --debug=0 --method_name=$METHODNAME --logdir=$LOGDIR-$dataset/$METHODNAME --seed=$i

    METHODNAME="Seq-Run6-Seed$i"
    echo "Running image $dataset with method: $METHODNAME"
    python main_script.py --dataset_nbr=$dataset --do_plot=0 --num_steps=100000 --learning_rate=0.01 --depth=5  \
    --non_linear=1 --layer_bias=1 --detrend=1 --learn_sigmasq=0 --use_constr_filter=0 --use_seq_filter=1 \
    --seq_filter_size=5 --debug=0 --method_name=$METHODNAME --logdir=$LOGDIR-$dataset/$METHODNAME --seed=$i    

    METHODNAME="Seq-Run7-Seed$i"
    echo "Running image $dataset with method: $METHODNAME"
    python main_script.py --dataset_nbr=$dataset --do_plot=0 --num_steps=100000 --learning_rate=0.01 --depth=5  \
    --non_linear=0 --layer_bias=1 --detrend=1 --learn_sigmasq=0 --use_constr_filter=0 --use_seq_filter=1 \
    --seq_filter_size=3 --debug=0 --method_name=$METHODNAME --logdir=$LOGDIR-$dataset/$METHODNAME --seed=$i        

    METHODNAME="Plus-Constr-Run1-Seed$i"
    echo "Running image $dataset with method: $METHODNAME"
    python main_script.py --dataset_nbr=$dataset --do_plot=0 --num_steps=100000 --learning_rate=0.01 --depth=5  \
    --non_linear=0 --layer_bias=1 --detrend=1 --learn_sigmasq=0 --use_constr_filter=1 --use_seq_filter=0 \
    --seq_filter_size=5 --debug=0 --method_name=$METHODNAME --logdir=$LOGDIR-$dataset/$METHODNAME --seed=$i    
done

wait
echo "done."
