#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Wed Jan 15 11:10:13 2020

Plot Paper fig 1
Inpainting results (posterior mean) for different methods on toy data

"""
#%%
import numpy as np
import scipy.io as spio
import matplotlib.pyplot as plt
import os
import sys
sys.path.append("..") 

from utils import (plot_toy_mask, get_toy_mask)
#%%

seed = 11
dpi= 1000
gmrf_nbrs = [1,4]
methods = ['ICAR','Plus1-Constr','Plus3-Constr','Seq3-3by3','Seq3-5by5']
titles = ['       data       ','   Matérn   ','    +,L=1    ','    +,L=3 ','    seq,3x3,L=3','seq,5x5,L=3']
M = len(gmrf_nbrs)
N = len(methods)
sz = [160,120]

for i in range(M):
    mat = spio.loadmat('data/indata/gmrf' + str(gmrf_nbrs[i]) + '.mat', squeeze_me=True)
    image = mat['GMRFsample']
    for j in range(N):
        if methods[j] == 'ICAR':
            mat = spio.loadmat('data/indata/postmean' + str(gmrf_nbrs[i]) + '.mat', squeeze_me=True)
            im_tmp = mat['mu_im']
        else:
            mat = spio.loadmat('data/outdata/results' + str(gmrf_nbrs[i]) + str(methods[j]) + '-Seed' + str(seed) + '.mat', squeeze_me=True)
            im_tmp = mat['mean']
        image = np.concatenate((image, im_tmp),1)     
    
    fig,ax = plt.subplots(1)
    im = ax.imshow(image)
    cbar = fig.colorbar(im,fraction=0.0146, pad=0.01, aspect = 15)
    cbar.ax.tick_params(labelsize=4)
    
    if i == 0:
        ax.set_title('          '.join(titles),pad=3,fontsize=6)
    for j in range(N):
        plt.axvline(x=(j+1)*sz[1]-.5,color='black',linewidth=0.5)    
    plot_toy_mask(sz,ax,0,0,edgecolor='none',facecolor='black')
    for j in range(1,N+1):
        plot_toy_mask(sz,ax,0,j,linewidth=0.3,linestyle="--",edgecolor='black',facecolor='none')
    ax.set_xticks([])
    ax.set_yticks([])
    ax.set_yticklabels([])
    ax.set_xticklabels([])
    # plt.show()
    plt.savefig('data/outdata/paper_fig1_' + str(gmrf_nbrs[i]) + '.png',bbox_inches='tight',dpi=dpi)
    plt.clf()