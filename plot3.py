#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Thu Jan 16 13:50:16 2020

Plot Paper fig 3
Satellite data

"""
#%%
import numpy as np
import scipy.io as spio
import matplotlib.pyplot as plt
import os
import sys
sys.path.append("..")
#%%

method = 'Seq-Run5'
seed = 15
dpi = 500
normalizing_factor = 55.41

mat1 = spio.loadmat('data/indata/gmrf52.mat', squeeze_me=True)
data = normalizing_factor*mat1["GMRFsample"]
mask = mat1["mask"]

mat2 = spio.loadmat('data/outdata/results52' + str(method) + '-Seed' + str(seed) + '.mat', squeeze_me=True)
postmean = normalizing_factor*mat2["mean"]
postsd = normalizing_factor*mat2["sd"]
postsamples = normalizing_factor*mat2["samples"]
postsample1 = postsamples[:,:300]
postsample2 = postsamples[:,300:]

# data
fig,ax = plt.subplots(1)
im = ax.imshow(np.transpose(np.float32(data)),vmin=20,vmax=60)
cbar = fig.colorbar(im,fraction=0.0194, pad=0.01, aspect = 30,ticks=[20,30,40,50,60])
cbar.ax.tick_params(labelsize=8)
ax.set_title('data',pad=3,fontsize=9)
ax.set_xticks([])
ax.set_yticks([])
ax.set_yticklabels([])
ax.set_xticklabels([])
# plt.show()
plt.savefig('data/outdata/paper_fig3_11.png',bbox_inches='tight',dpi=dpi)
plt.clf()

# mask
fig,ax = plt.subplots(1)
im = ax.imshow(np.transpose(10*np.float32(1.0-mask)))
cbar = fig.colorbar(im,fraction=0.0194, pad=0.01, aspect = 30,ticks=[0,10])#, ticks = [-20,-10,0,10,20])
cbar.ax.tick_params(labelsize=8,labelcolor='w')
ax.set_title('missing pixels',pad=3,fontsize=9)
ax.set_xticks([])
ax.set_yticks([])
ax.set_yticklabels([])
ax.set_xticklabels([])
# plt.show()
plt.savefig('data/outdata/paper_fig3_12.png',bbox_inches='tight',dpi=dpi)
plt.clf()

# post mean
fig,ax = plt.subplots(1)
im = ax.imshow(np.transpose(postmean),vmin=20,vmax=60)
cbar = fig.colorbar(im,fraction=0.0194, pad=0.01, aspect = 30,ticks=[20,30,40,50,60])
cbar.ax.tick_params(labelsize=8)
ax.set_title('posterior mean',pad=3,fontsize=9)
ax.set_xticks([])
ax.set_yticks([])
ax.set_yticklabels([])
ax.set_xticklabels([])
# plt.show()
plt.savefig('data/outdata/paper_fig3_21.png',bbox_inches='tight',dpi=dpi)
plt.clf()

# post sd
fig,ax = plt.subplots(1)
im = ax.imshow(np.transpose(postsd))
cbar = fig.colorbar(im,fraction=0.0194, pad=0.01, aspect = 30)#, ticks = [-20,-10,0,10,20])
cbar.ax.tick_params(labelsize=8,pad=9)
ax.set_title('posterior sd',pad=3,fontsize=9)
ax.set_xticks([])
ax.set_yticks([])
ax.set_yticklabels([])
ax.set_xticklabels([])
# plt.show()
plt.savefig('data/outdata/paper_fig3_22.png',bbox_inches='tight',dpi=dpi)
plt.clf()

# post sample1
fig,ax = plt.subplots(1)
im = ax.imshow(np.transpose(postsample1),vmin=20,vmax=60)
cbar = fig.colorbar(im,fraction=0.0194, pad=0.01, aspect = 30,ticks=[20,30,40,50,60])
cbar.ax.tick_params(labelsize=8)
ax.set_title('posterior sample 1',pad=3,fontsize=9)
ax.set_xticks([])
ax.set_yticks([])
ax.set_yticklabels([])
ax.set_xticklabels([])
# plt.show()
plt.savefig('data/outdata/paper_fig3_31.png',bbox_inches='tight',dpi=dpi)
plt.clf()

# post sample2
fig,ax = plt.subplots(1)
im = ax.imshow(np.transpose(postsample2),vmin=20,vmax=60)
cbar = fig.colorbar(im,fraction=0.0194, pad=0.01, aspect = 30,ticks=[20,30,40,50,60])
cbar.ax.tick_params(labelsize=8)
ax.set_title('posterior sample 2',pad=3,fontsize=9)
ax.set_xticks([])
ax.set_yticks([])
ax.set_yticklabels([])
ax.set_xticklabels([])
# plt.show()
plt.savefig('data/outdata/paper_fig3_32.png',bbox_inches='tight',dpi=dpi)
plt.clf()