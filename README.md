# README #

This code is a supplement to the 2020 ICML article Deep Gaussian Markov random fields [1].

* main_script.py contains the main script for defining the deep GMRF, training and posterior inference.
* run1.sh runs the main script for various settings and can be used to reproduce the toy data results in Fig. 2 and Fig. 3.
* run2.sh runs the main script for various settings for the satellite data and can be used to reproduce the results in Table 1, Table 2 and Fig. 4.
* plot1.py produces Fig. 2.
* plot3.py produces Fig. 4.
* eval_Heaton_results.R produces Table 1 and Table 2.
* The Matlab (.m) files can be used to simulate the toy data, and import_Heaton_data.R preprocesses the satellite data linked in the paper.
* inpainting_DIP.py runs Deep image prior (DIP) [2] and this file should be copied into that repository to work.

[1]  Sidén, P., & Lindsten, F. (2020). Deep Gaussian Markov random fields, In Proceedings of the 37th International Conference on Machine Learning (ICML).

[2] https://github.com/DmitryUlyanov/deep-image-prior
