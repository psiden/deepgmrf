#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Mon Apr 13 17:08:31 2020

Do satellite inpainting task using Deep image prior
Rewritten based on the deep image prior inpainting notebook
https://github.com/DmitryUlyanov/deep-image-prior/blob/master/inpainting.ipynb

@author: per siden
"""

#%% Import libs
from __future__ import print_function
import matplotlib.pyplot as plt
# %matplotlib inline

import os
# os.environ['CUDA_VISIBLE_DEVICES'] = '1'

import numpy as np
from models.resnet import ResNet
from models.unet import UNet
from models.skip import skip
import torch
import torch.optim
import scipy.io as spio
import argparse

from utils.inpainting_utils import *

torch.backends.cudnn.enabled = True
torch.backends.cudnn.benchmark =True
dtype = torch.cuda.FloatTensor

PLOT = False # True #
imsize = -1
dim_div_by = 64

SEED = 2
METHOD_NAME = 'vase'


#%% Get arguments function
def get_arguments():
    parser = argparse.ArgumentParser(description='ConvNet training')
    parser.add_argument('--seed', type=int, default=SEED,
                        help='Random number generator seed.')
    parser.add_argument('--method_name', type=str, default=METHOD_NAME,
                        help='Method name for output files.')
    return parser.parse_args()

#%% Choose figure

## Fig 6
# img_path  = 'data/inpainting/vase.png'
# mask_path = 'data/inpainting/vase_mask.png'

## Fig 8
# img_path  = 'data/inpainting/library.png'
# mask_path = 'data/inpainting/library_mask.png'

## Fig 7 (top)
# img_path  = 'data/inpainting/kate.png'
# mask_path = 'data/inpainting/kate_mask.png'

# Another text inpainting example
# img_path  = 'data/inpainting/peppers.png'
# mask_path = 'data/inpainting/peppers_mask.png'

args = get_arguments()
seed = args.seed
dataset_nbr = 55
img_path = args.method_name
# img_path = 'vase'
# img_path = 'kate'
# img_path = 'library'
pad_width = [6,10]
np.random.seed(seed)

mat = spio.loadmat('data/indata/gmrf' + str(dataset_nbr) + '.mat', squeeze_me=True)
img_np = mat['GMRFsample']
img_np = np.nan_to_num(img_np,nan=-0000)
# img_np = np.transpose(img_np)
# img_np = img_np[:448,:256]
img_np = np.pad(img_np,((pad_width[0],pad_width[0]),(pad_width[1],pad_width[1])))
sz = img_np.shape
img_np = np.reshape(img_np,[1,sz[0],sz[1]])
# img_np = np.concatenate([img_np,img_np,img_np],0)
img_np = img_np.astype('float32')
img_mask_np = mat['mask']
# img_mask_np = img_mask_np[:448,:256]
# img_mask_np = np.ones_like(img_mask_np)
# img_mask_np[200,200] = 0
img_mask_np = np.pad(img_mask_np,((6,6),(10,10)))
img_mask_np = np.reshape(img_mask_np,[1,sz[0],sz[1]])
img_mask_np = img_mask_np.astype('float32')


NET_TYPE = 'skip_depth6' # one of skip_depth6|skip_depth4|skip_depth2|UNET|ResNet

#%% Load mask

# img_pil, img_np1 = get_image(img_path, imsize)
# img_mask_pil, img_mask_np = get_image(mask_path, imsize)

#%% Center crop

# img_mask_pil = crop_image(img_mask_pil, dim_div_by)
# img_pil      = crop_image(img_pil,      dim_div_by)
# img_np      = pil_to_np(img_pil)
# img_mask_np = pil_to_np(img_mask_pil)

#%% Visualize

# img_mask_var = np_to_torch(img_mask_np).type(dtype)
# plot_image_grid([img_np, img_mask_np, img_mask_np*img_np], 3,11);

#%% Setup

pad = 'reflection' # 'zero'
OPT_OVER = 'net'
OPTIMIZER = 'adam'

if 'gmrf' in img_path:
    INPUT = 'noise'
    input_depth = 32
    LR = 0.01 
    num_iter = 1001 # 1001
    param_noise = False
    show_every = 50
    figsize = 5
    reg_noise_std = 0.03
    
    net = skip(input_depth, img_np.shape[0], 
                num_channels_down = [128] * 5,
                num_channels_up =   [128] * 5,
                num_channels_skip =    [128] * 5,  
                filter_size_up = 3, filter_size_down = 3, 
                upsample_mode='nearest', filter_skip_size=1,
                need_sigmoid=True, need_bias=True, pad=pad, act_fun='LeakyReLU').type(dtype)
elif 'vase' in img_path:
    INPUT = 'meshgrid'
    input_depth = 2
    LR = 0.01 
    num_iter = 5001
    param_noise = False
    show_every = 50
    figsize = 5
    reg_noise_std = 0.03
    
    net = skip(input_depth, img_np.shape[0], 
                num_channels_down = [128] * 5,
                num_channels_up   = [128] * 5,
                num_channels_skip = [0] * 5,  
                upsample_mode='nearest', filter_skip_size=1, filter_size_up=3, filter_size_down=3,
                need_sigmoid=True, need_bias=True, pad=pad, act_fun='LeakyReLU').type(dtype)
    
elif ('kate' in img_path) or ('peppers.png' in img_path):
    # Same params and net as in super-resolution and denoising
    INPUT = 'noise'
    input_depth = 32
    LR = 0.01 
    num_iter = 6001
    param_noise = False
    show_every = 50
    figsize = 5
    reg_noise_std = 0.03
    
    net = skip(input_depth, img_np.shape[0], 
                num_channels_down = [128] * 5,
                num_channels_up =   [128] * 5,
                num_channels_skip =    [128] * 5,  
                filter_size_up = 3, filter_size_down = 3, 
                upsample_mode='nearest', filter_skip_size=1,
                need_sigmoid=True, need_bias=True, pad=pad, act_fun='LeakyReLU').type(dtype)
    
elif 'library' in img_path:
    
    INPUT = 'noise'
    input_depth = 1
    
    num_iter = 3001
    show_every = 50
    figsize = 8
    reg_noise_std = 0.00
    param_noise = True
    
    if 'skip' in NET_TYPE:
        
        depth = int(NET_TYPE[-1])
        net = skip(input_depth, img_np.shape[0], 
                num_channels_down = [16, 32, 64, 128, 128, 128][:depth],
                num_channels_up =   [16, 32, 64, 128, 128, 128][:depth],
                num_channels_skip =    [0, 0, 0, 0, 0, 0][:depth],  
                filter_size_up = 3,filter_size_down = 5,  filter_skip_size=1,
                upsample_mode='nearest', # downsample_mode='avg',
                need1x1_up=False,
                need_sigmoid=True, need_bias=True, pad=pad, act_fun='LeakyReLU').type(dtype)
        
        LR = 0.01 
        
    elif NET_TYPE == 'UNET':
        
        net = UNet(num_input_channels=input_depth, num_output_channels=3, 
                    feature_scale=8, more_layers=1, 
                    concat_x=False, upsample_mode='deconv', 
                    pad='zero', norm_layer=torch.nn.InstanceNorm2d, need_sigmoid=True, need_bias=True)
        
        LR = 0.001
        param_noise = False
        
    elif NET_TYPE == 'ResNet':
        
        net = ResNet(input_depth, img_np.shape[0], 8, 32, need_sigmoid=True, act_fun='LeakyReLU')
        
        LR = 0.001
        param_noise = False
        
    else:
        assert False
else:
    assert False

net = net.type(dtype)
net_input = get_noise(input_depth, INPUT, img_np.shape[1:]).type(dtype)

#%% Compute number of parameters
s  = sum(np.prod(list(p.size())) for p in net.parameters())
print ('Number of params: %d' % s)

# Loss
mse = torch.nn.MSELoss().type(dtype)

img_var = np_to_torch(img_np).type(dtype)
mask_var = np_to_torch(img_mask_np).type(dtype)

#%% Main loop
i = 0
def closure():
    
    global i
    
    if param_noise:
        for n in [x for x in net.parameters() if len(x.size()) == 4]:
            n = n + n.detach().clone().normal_() * n.std() / 50
    
    net_input = net_input_saved
    if reg_noise_std > 0:
        net_input = net_input_saved + (noise.normal_() * reg_noise_std)
        
        
    out = net(net_input)
   
    total_loss = mse(out * mask_var, img_var * mask_var)
    total_loss.backward()
        
    print ('Iteration %05d    Loss %f' % (i, total_loss.item()), '\r', end='')
    if  PLOT and i % show_every == 0:
        out_np = torch_to_np(out)
        plot_image_grid([np.clip(out_np, 0, 1)], factor=figsize, nrow=1)
        
    i += 1

    return total_loss

net_input_saved = net_input.detach().clone()
noise = net_input.detach().clone()

p = get_params(OPT_OVER, net, net_input)
optimize(OPTIMIZER, p, closure, LR, num_iter)

#%%
out_np = torch_to_np(net(net_input))
# plot_image_grid([out_np], factor=5);

spio.savemat('data/outdata/deep_image_prior-' + img_path + str(seed) + '.mat', \
                 {"mean":np.squeeze(out_np)})

