echo "Running python code..."

LOGDIR="logs/log"

for i in {11..11}; do
    dataset=1
    METHODNAME="Plus1-Constr-Seed$i"
    echo "Running image $dataset with method: $METHODNAME"
    python main_script.py --dataset_nbr=$dataset --do_plot=0 --num_steps=100000 --learning_rate=0.01 --depth=1  \
    --non_linear=0 --layer_bias=0 --detrend=0 --learn_sigmasq=0 --use_constr_filter=1 --use_seq_filter=0 \
    --seq_filter_size=5 --debug=0 --method_name=$METHODNAME --logdir=$LOGDIR-$dataset/$METHODNAME --seed=$i
    
    dataset=1
    METHODNAME="Plus3-Constr-Seed$i"
    echo "Running image $dataset with method: $METHODNAME"
    python main_script.py --dataset_nbr=$dataset --do_plot=0 --num_steps=100000 --learning_rate=0.01 --depth=3  \
    --non_linear=0 --layer_bias=0 --detrend=0 --learn_sigmasq=0 --use_constr_filter=1 --use_seq_filter=0 \
    --seq_filter_size=5 --debug=0 --method_name=$METHODNAME --logdir=$LOGDIR-$dataset/$METHODNAME --seed=$i
    
    dataset=4
    METHODNAME="Plus1-Constr-Seed$i"
    echo "Running image $dataset with method: $METHODNAME"
    python main_script.py --dataset_nbr=$dataset --do_plot=0 --num_steps=100000 --learning_rate=0.01 --depth=1  \
    --non_linear=0 --layer_bias=0 --detrend=0 --learn_sigmasq=0 --use_constr_filter=1 --use_seq_filter=0 \
    --seq_filter_size=5 --debug=0 --method_name=$METHODNAME --logdir=$LOGDIR-$dataset/$METHODNAME --seed=$i
    
    dataset=4
    METHODNAME="Plus3-Constr-Seed$i"
    echo "Running image $dataset with method: $METHODNAME"
    python main_script.py --dataset_nbr=$dataset --do_plot=0 --num_steps=100000 --learning_rate=0.01 --depth=3  \
    --non_linear=0 --layer_bias=0 --detrend=0 --learn_sigmasq=0 --use_constr_filter=1 --use_seq_filter=0 \
    --seq_filter_size=5 --debug=0 --method_name=$METHODNAME --logdir=$LOGDIR-$dataset/$METHODNAME --seed=$i
    
    dataset=1
    METHODNAME="Seq3-3by3-Seed$i"
    echo "Running image $dataset with method: $METHODNAME"
    python main_script.py --dataset_nbr=$dataset --do_plot=0 --num_steps=100000 --learning_rate=0.01 --depth=3  \
    --non_linear=0 --layer_bias=0 --detrend=0 --learn_sigmasq=0 --use_constr_filter=0 --use_seq_filter=1 \
    --seq_filter_size=3 --debug=0 --method_name=$METHODNAME --logdir=$LOGDIR-$dataset/$METHODNAME --seed=$i 
    
    dataset=1
    METHODNAME="Seq3-5by5-Seed$i"
    echo "Running image $dataset with method: $METHODNAME"
    python main_script.py --dataset_nbr=$dataset --do_plot=0 --num_steps=100000 --learning_rate=0.01 --depth=3  \
    --non_linear=0 --layer_bias=0 --detrend=0 --learn_sigmasq=0 --use_constr_filter=0 --use_seq_filter=1 \
    --seq_filter_size=5 --debug=0 --method_name=$METHODNAME --logdir=$LOGDIR-$dataset/$METHODNAME --seed=$i
    
    dataset=4
    METHODNAME="Seq3-3by3-Seed$i"
    echo "Running image $dataset with method: $METHODNAME"
    python main_script.py --dataset_nbr=$dataset --do_plot=0 --num_steps=100000 --learning_rate=0.01 --depth=3  \
    --non_linear=0 --layer_bias=0 --detrend=0 --learn_sigmasq=0 --use_constr_filter=0 --use_seq_filter=1 \
    --seq_filter_size=3 --debug=0 --method_name=$METHODNAME --logdir=$LOGDIR-$dataset/$METHODNAME --seed=$i
    
    dataset=4
    METHODNAME="Seq3-5by5-Seed$i"
    echo "Running image $dataset with method: $METHODNAME"
    python main_script.py --dataset_nbr=$dataset --do_plot=0 --num_steps=100000 --learning_rate=0.01 --depth=3  \
    --non_linear=0 --layer_bias=0 --detrend=0 --learn_sigmasq=0 --use_constr_filter=0 --use_seq_filter=1 \
    --seq_filter_size=5 --debug=0 --method_name=$METHODNAME --logdir=$LOGDIR-$dataset/$METHODNAME --seed=$i    
done


wait
echo "done."
