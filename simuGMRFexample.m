%% %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% PURPOSE:      Example simulation of GMRF CAR(2) 
%
% FIRST VER.:   2017-01-25
% REVISED:      2019-09-05
%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%% Simulate parameters from spatial priors
clear all
close all
clc
rng(102);

sz = [300,300]; % [200,200]; %
szOut = [160,120]; % [100,100]; % % Output midpoint window
tau2 = 1;
range = 50;
kappa2 = 8/(range^2); % 0; %
alpha = 2;
use_ichol = 0;

N = prod(sz);
bmask = 1:N;
ndim = length(sz);

if alpha == 1
  QTypes = {'LI'};
  [QList,GQList] = setupPrecMats(QTypes,N,sz,bmask,ndim);
  GQ = GQList{1};
  Q = tau2*(kappa2*speye(N) + GQ'*GQ);
  NGQ = size(GQ,1);
  b = sqrt(tau2) * (sqrt(kappa2) * randn(N,1) + GQ' * randn(NGQ,1));
elseif alpha == 2
  QTypes = {'LI'};
  [QList,GQList] = setupPrecMats(QTypes,N,sz,bmask,ndim);
  GQ = sqrt(tau2) * (QList{1} + kappa2*speye(N));
%   Q = GQ'*GQ;
%   NGQ = size(GQ,1);
%   b = GQ' * randn(NGQ,1);
  Q = GQ; % Works as well
  b = randn(N,1);
end

QI = amd(Q);
QP = Q(QI,QI);
bP = b(QI);
if use_ichol
  M1 = ichol(QP);
  [wP,flag,relres] = pcg(QP,bP,1e-9,1000,M1,M1',zeros(N,1));
else
  [wP,flag,relres] = pcg(QP,bP,1e-9,1000);
end
wTemp = zeros(N,1);
wTemp(QI) = wP;
w = wTemp';

disp(strcat("Sampling error: ",num2str(relres)))
GMRFsample = reshape(w,sz);
GMRFsample = GMRFsample(floor(sz(1)/2-szOut(1)/2)+1:floor(sz(1)/2-szOut(1)/2)+szOut(1),...
                        floor(sz(2)/2-szOut(2)/2)+1:floor(sz(2)/2-szOut(2)/2)+szOut(2));
                      
% Add edges
% edges1 = 2*kron([3,-1;-1,-1],ones(50));
% edges2 = 3*kron([1,-1,1,-1,1,-1,1,-1,1,-1],ones(100,10));
% edges3 = 3*kron([1,-1,1,-1,1,-1,1,-1,1,-1],ones(szOut(1),szOut(2)/10));
% e4_1 = kron([1,-1,1,-1]',ones(szOut(1)/16,szOut(2)));
% e4_2 = kron([1,-1,1,-1,1,-1,1,-1],ones(szOut(1)/2,szOut(2)/8));
% edges4 = 3*[e4_1;e4_2;e4_1];
% GMRFsample = GMRFsample + edges4;
% GMRFsample = rand(size(GMRFsample)); % Dummy output

figure(2);
imagesc(GMRFsample);colorbar;axis image;
title(strcat("CAR(",num2str(alpha),") with range ",num2str(sqrt(8/kappa2))));

% save('data/indata/gmrf1.mat','GMRFsample')
% % imwrite(GMRFsample,'GMRFsample.png')

