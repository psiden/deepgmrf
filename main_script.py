#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Mon Nov 18 14:45:47 2019

Main script for deep GMRF learning and posterior inference
"""

#%% Import libraries
import tensorflow as tf
import numpy as np
import scipy.io as spio
import matplotlib.pyplot as plt
import argparse
from datetime import datetime
import tensorflow_probability as tfp
tfd = tfp.distributions
import os

from utils import (
    mean_squared_error_hadamard, lrelu_deri, lrelu_channelwise,
    get_toy_mask, plot_toy_mask, pad_image, pad_lon, pad_lat)

#%% Default/Debug arguments
SEED = 11
NUM_STEPS = int(1e2)
DATASET_NBR = 4
DO_PLOT = 0
LEARNING_RATE = 0.01

METHOD_NAME = 'test' # 'Seq-Run1-Seed11' #
DEPTH = 1
NON_LINEAR = 0
LAYER_BIAS = 0
DETREND = 0
LEARN_SIGMASQ = 0
USE_CONSTR_FILTER = 0
USE_SEQ_FILTER = 1
SEQ_FILTER_SIZE = 3
CG_TOL_LOG10 = -7
CG_MIN_ITER = int(1e3)
CG_MAX_ITER = int(1e4)

CHECKPOINT_EVERY = 100
LOGDIR = 'logs/log-' + str(DATASET_NBR) + '/' + METHOD_NAME
CKPT_FILE = 'model.ckpt'
RESTORE_FROM = None # 'latest' #'model-step-90000-loss-1.92918.ckpt' #
DO_OPTIM = 1
NBR_POSTERIOR_SAMPLES = 100
DEBUG = 1

# RESTORE_FROM = 'latest'
# DO_OPTIM = 0
# NBR_POSTERIOR_SAMPLES = 2
# DEBUG = 0

#%% Get arguments function
def get_arguments():
    parser = argparse.ArgumentParser(description='ConvNet training')
    parser.add_argument('--seed', type=int, default=SEED,
                        help='Random number generator seed.')
    parser.add_argument('--num_steps', type=int, default=NUM_STEPS,
                        help='Number of training steps.')
    parser.add_argument('--do_plot', type=int, default=DO_PLOT,
                        help='Should plots be displayed?')
    parser.add_argument('--dataset_nbr', type=int, default=DATASET_NBR,
                         help='Input data file number.')
    parser.add_argument('--learning_rate', type=float, default=LEARNING_RATE,
                         help='Learning rate for training.')
    parser.add_argument('--method_name', type=str, default=METHOD_NAME,
                        help='Method name for output files.')
    parser.add_argument('--logdir', type=str, default=LOGDIR,
                        help='Directory for log files.')
    parser.add_argument('--restore_from', type=str, default=RESTORE_FROM,
                        help='Checkpoint file to restore model weights from.')
    parser.add_argument('--checkpoint_every', type=int, default=CHECKPOINT_EVERY,
                        help='How many steps to save each checkpoint after')
    parser.add_argument('--do_optim', type=int, default=DO_OPTIM,
                        help='Should the cnn optimization run?')
    parser.add_argument('--nbr_posterior_samples', type=int, default=NBR_POSTERIOR_SAMPLES,
                        help='Number of posterior samples to draw after the optimization')
    parser.add_argument('--depth', type=int, default=DEPTH,
                        help='Number of convolutional layers')
    parser.add_argument('--non_linear', type=int, default=NON_LINEAR,
                        help='Should the cnn be non-linear?')
    parser.add_argument('--layer_bias', type=int, default=LAYER_BIAS,
                        help='Should each layer have a bias?')
    parser.add_argument('--detrend', type=int, default=DETREND,
                        help='Should there be a linear trend model?')
    parser.add_argument('--learn_sigmasq', type=int, default=LEARN_SIGMASQ,
                        help='Should sigma_sq be a learnable parameter?')
    parser.add_argument('--use_constr_filter', type=int, default=USE_CONSTR_FILTER,
                        help='Should constrained +-filters be used?')
    parser.add_argument('--use_seq_filter', type=int, default=USE_SEQ_FILTER,
                        help='Should sequential filters be used? (+-filters otherwise)')
    parser.add_argument('--seq_filter_size', type=int, default=SEQ_FILTER_SIZE,
                        help='Size of each sequential filter.')
    parser.add_argument('--cg_tol_log10', type=int, default=CG_TOL_LOG10,
                        help='log10 of conjugate gradient tolerance.')
    parser.add_argument('--cg_min_iter', type=int, default=CG_MIN_ITER,
                        help='Conjugate gradient minimum iterations.')
    parser.add_argument('--cg_max_iter', type=int, default=CG_MAX_ITER,
                        help='Conjugate gradient maximum iterations.')
    parser.add_argument('--debug', type=int, default=DEBUG,
                        help='Should we make a quick run in debug mode?')
    return parser.parse_args()


#%% Load image

args = get_arguments()
seed = args.seed
np.random.seed(seed)
tf.compat.v1.reset_default_graph()

mat = spio.loadmat('data/indata/gmrf' + str(args.dataset_nbr) + '.mat', squeeze_me=True)
image = mat['GMRFsample']
image = np.nan_to_num(image,nan=-0000)
#image = image[20:40,20:30]
sz = image.shape
N = sz[0]*sz[1]

pad_width = 10 # Width of image extension
sz_pad = (sz[0]+2*pad_width,sz[1]+2*pad_width)
N_pad = sz_pad[0]*sz_pad[1]
if len(sz) == 2:
    C = 1 # Nbr of channels
    image = np.reshape(image,[sz[0],sz[1],1])
else:
    C = sz[2]
image_pad = tf.constant(pad_image(image,pad_width),dtype=tf.float32)

if "mask" in mat:
    mask = np.reshape(mat['mask'],[sz[0],sz[1],C])
else:
    mask = np.reshape(get_toy_mask(sz),[sz[0],sz[1],C])
if "val_mask" in mat:
    val_mask = np.reshape(mat['val_mask'],[sz[0],sz[1],C])
else:
    val_mask = 1-mask
    
M = np.sum(mask)
mask_pad = tf.constant(pad_image(mask,pad_width),dtype=tf.float32)
val_mask_pad = tf.constant(pad_image(val_mask,pad_width),dtype=tf.float32)

if args.detrend:  
    if "lon" in mat:
        lon_orig = mat['lon']
        lon_mean = np.mean(lon_orig)
        lon_sd = np.std(lon_orig)
        lon = (lon_orig-lon_mean)/lon_sd
        lon_pad = pad_lon(lon,pad_width)
    if "lat" in mat:
        lat_orig = mat['lat']
        lat_mean = np.mean(lat_orig)
        lat_sd = np.std(lat_orig)
        lat = (lat_orig-lat_mean)/lat_sd
        lat_pad = pad_lat(lat,pad_width)

if args.do_plot:
    plt.imshow(np.squeeze(image))
    plt.colorbar()
    plt.show()

#%% Define Classes

pi = tf.constant(np.pi)
eig_cosvec0 = tf.cos(pi/(sz_pad[0]+1.0)*tf.range(1,sz_pad[0]+1,dtype=tf.float32))
eig_cosvec1 = tf.cos(pi/(sz_pad[1]+1.0)*tf.range(1,sz_pad[1]+1,dtype=tf.float32))
eig_cosvec = [eig_cosvec0,eig_cosvec1]
              
class SeqFilter(object):
    
    # Sequential filter, possibly followed by a non-linearity
    # Permutation of:
    #  0  0  0
    #  0 a0 a1
    # a2 a3 a4
    
    def __init__(self,perm=0,size=3,nonlinear=1,alpha=1.0):
        
        self.perm = perm % 8
        self.size = size
        param_len = (size*size+1) // 2
        
        self.a = tf.Variable(tf.random.truncated_normal([param_len,1], stddev=.01))
        w_tmp = tf.reshape(tf.concat([tf.zeros((param_len-1,1)),self.a],0),(size,size))
        if self.perm==0:
            self.w = w_tmp
        elif self.perm==1:
            self.w = tf.reverse(tf.transpose(w_tmp),[0])
        elif self.perm==2:
            self.w = tf.reverse(w_tmp,[0,1])
        elif self.perm==3:
            self.w = tf.transpose(tf.reverse(w_tmp,[1]))
        elif self.perm==4:
            self.w = tf.reverse(w_tmp,[1])
        elif self.perm==5:
            self.w = tf.transpose(w_tmp)
        elif self.perm==6:
            self.w = tf.reverse(w_tmp,[0])
        elif self.perm==7:  
            self.w = tf.transpose(tf.reverse(w_tmp,[0,1]))
            
        self.filter = tf.reshape(self.w,(size,size,1,1))
        if nonlinear == 1:
            self.alpha0 = tf.Variable(tf.constant(np.log(np.exp(alpha)-1),tf.float32))
            self.alpha = tf.math.softplus(self.alpha0)
        else:
            self.alpha = tf.constant(alpha,tf.float32)
        
    def get_filter(self):
        return self.filter
    
    def get_alpha(self):
        return self.alpha
    
    def mean_log_det(self,x):
        return tf.log(tf.math.maximum(tf.abs(self.a[0]),tf.constant(1e-12))) + \
               tf.reduce_mean(tf.log(lrelu_deri(x,self.alpha)))
               
class OneByThreeFilter(object):
    
    # 1 by 3 filter 
    # c a b
    
    def __init__(self,dim):  
        self.dim = dim   
        self.a = tf.Variable(tf.constant(2.0,tf.float32))
        self.b = tf.Variable(tf.constant(.5-np.random.rand(1)[0],tf.float32))
        self.c = tf.Variable(tf.constant(.5-np.random.rand(1)[0],tf.float32))
#        self.a = tf.Variable(tf.constant(np.random.normal(0,.01),tf.float32))
#        self.b = tf.Variable(tf.constant(np.random.normal(0,.01),tf.float32))
#        self.c = tf.Variable(tf.constant(np.random.normal(0,.01),tf.float32))
        self.w = tf.stack([self.c,self.a,self.b],0)
        self.aC = tf.complex(self.a,0.0)
        self.bC = tf.complex(self.b,0.0)
        self.cC = tf.complex(self.c,0.0)
        
    def get_filter(self):
        return self.w
    
    def eig(self):
        return self.aC + 2.0*tf.sqrt(self.bC*self.cC)*tf.dtypes.cast(eig_cosvec[self.dim],tf.complex64)
        
class ConstrainedOneByThreeFilter(object):
    
    # 1 by 3 filter 
    # c a b
    #
    # a = softplus(a0)
    # Requirement: 0 < 2*sqrt(b*c) < a
    # f^2 = b*c, g = b/c
    # gives 
    # b = f*sqrt(g), c = f/sqrt(g)
    # f = (a/2)*tanh(f0), g = exp(g0)
    
    def __init__(self,dim,a0=2.0,f0=-0.1,g0=0.0):  
        self.dim = dim
        self.a0 = tf.Variable(tf.constant(a0,tf.float32))
        self.a = tf.math.softplus(self.a0) 
        self.f0 = tf.Variable(tf.constant(f0,tf.float32))
        self.g0 = tf.Variable(tf.constant(g0,tf.float32))
        self.f = (self.a / 2.0) * tf.tanh(self.f0)
        self.g = tf.exp(self.g0)
        self.b = self.f * tf.sqrt(self.g)
        self.c = self.f / tf.sqrt(self.g)
        self.w = tf.stack([self.c,self.a,self.b],0)
        
    def get_filter(self):
        return self.w
    
    def eig(self):
        return self.a + 2.0*self.f*eig_cosvec[self.dim]  
    
class ConstrainedPlusFilter(object):
    
    # Plus (Kronecker sum) filter, possibly followed by a non-linearity
    #
    #     c1
    # c2 a1+a2 b2
    #     b1
    
    def __init__(self,nonlinear=1,alpha=1.0):
        
        self.filter0 = ConstrainedOneByThreeFilter(0)
        self.filter1 = ConstrainedOneByThreeFilter(1)
        self.w1 = tf.transpose(tf.stack([tf.constant([0.0,0.0,0.0]),self.filter0.get_filter(),tf.constant([0.0,0.0,0.0])],0))
        self.w2 = tf.stack([tf.constant([0.0,0.0,0.0]),self.filter1.get_filter(),tf.constant([0.0,0.0,0.0])],0)
        self.filter = tf.reshape(self.w1 + self.w2,(3,3,1,1))
        if nonlinear == 1:
            self.alpha0 = tf.Variable(tf.constant(np.log(np.exp(alpha)-1),tf.float32))
            self.alpha = tf.math.softplus(self.alpha0)
        else:
            self.alpha = tf.constant(alpha,tf.float32)
        
    def get_filter(self):
        return self.filter
    
    def get_alpha(self):
        return self.alpha
    
    def eig(self):
        return tf.transpose([self.filter0.eig()]) + self.filter1.eig()
    
    def mean_log_det(self,x):
        return tf.reduce_mean(tf.log(tf.math.maximum(tf.abs(self.eig()),tf.constant(1e-12)))) + \
               tf.reduce_mean(tf.log(lrelu_deri(x,self.alpha)))
               
class PlusFilter(object):
    
    # Plus (Kronecker sum) filter, possibly followed by a non-linearity
    #
    #     a2
    # a1  a0  a3
    #     a4
    
    def __init__(self,nonlinear=1,alpha=1.0):
        
        self.a = tf.Variable(tf.random.truncated_normal([5,1], stddev=.1))
        self.w = tf.reshape(tf.stack([tf.zeros(1),self.a[2],tf.zeros(1),self.a[1],self.a[0],self.a[3], \
                                       tf.zeros(1),self.a[4],tf.zeros(1)]),(3,3))
        self.filter = tf.reshape(self.w,(3,3,1,1))
        if nonlinear == 1:
            self.alpha0 = tf.Variable(tf.constant(np.log(np.exp(alpha)-1),tf.float32))
            self.alpha = tf.math.softplus(self.alpha0)
        else:
            self.alpha = tf.constant(alpha,tf.float32)
        self.aC = tf.complex(self.a,tf.zeros_like(self.a))
        
    def get_filter(self):
        return self.filter
    
    def get_alpha(self):
        return self.alpha
    
    def eig(self):
        eig0 = .5*self.aC[0] + 2.0*tf.sqrt(self.aC[2]*self.aC[4])*tf.dtypes.cast(eig_cosvec[0],tf.complex64)
        eig1 = .5*self.aC[0] + 2.0*tf.sqrt(self.aC[1]*self.aC[3])*tf.dtypes.cast(eig_cosvec[1],tf.complex64)
        return tf.transpose([eig0]) + eig1
    
    def mean_log_det(self,x):
        return tf.reduce_mean(tf.log(tf.math.maximum(tf.abs(self.eig()),tf.constant(1e-12)))) + \
               tf.reduce_mean(tf.log(lrelu_deri(x,self.alpha)))
               
class KronProdFilter(object):
    
    # Kronecker prod filter, possibly followed by a non-linearity
    #
    # c1c2 c1a2 c1b2
    # a1c2 a1a2 a1b2
    # b1c2 b1a2 b1b2
    
    def __init__(self,nonlinear=1,alpha=1.0):
        
        self.filter0 = OneByThreeFilter(0)
        self.filter1 = OneByThreeFilter(1)
        self.w1 = tf.reshape(self.filter0.get_filter(),(3,1))
        self.w2 = tf.reshape(self.filter1.get_filter(),(1,3))
        self.w = tf.matmul(self.w1,self.w2)
        self.filter = tf.reshape(self.w,(3,3,1,1))
        if nonlinear == 1:
            self.alpha0 = tf.Variable(tf.constant(np.log(np.exp(alpha)-1),tf.float32))
            self.alpha = tf.math.softplus(self.alpha0)
        else:
            self.alpha = tf.constant(alpha,tf.float32)
        
    def get_filter(self):
        return self.filter
    
    def get_alpha(self):
        return self.alpha
    
    def eig(self):
        return tf.transpose([self.filter0.eig()]) * self.filter1.eig()
    
    def mean_log_det(self,x):
        return tf.reduce_mean(tf.log(tf.math.maximum(tf.abs(self.eig()),tf.constant(1e-12)))) + \
               tf.reduce_mean(tf.log(lrelu_deri(x,self.alpha)))
    
class ScaleFilter(object):
    
    # Scaling with a, possibly followed by a non-linearity
    
    def __init__(self,size=3,nonlinear=1,alpha=1.0):
        
        self.size = size
        param_len = (size*size+1) // 2
        
        self.a = tf.Variable(tf.random.truncated_normal([1,1], stddev=.01))
        self.w = tf.reshape(tf.concat([tf.zeros((param_len-1,1)),self.a,tf.zeros((param_len-1,1))],0),(size,size))
        self.filter = tf.reshape(self.w,(size,size,1,1))
        if nonlinear == 1:
            self.alpha0 = tf.Variable(tf.constant(np.log(np.exp(alpha)-1),tf.float32))
            self.alpha = tf.math.softplus(self.alpha0)
        else:
            self.alpha = tf.constant(alpha,tf.float32)
        
    def get_filter(self):
        return self.filter
    
    def get_alpha(self):
        return self.alpha
    
    def mean_log_det(self,x):
        return tf.log(tf.math.maximum(tf.abs(self.a[0]),tf.constant(1e-12))) + \
               tf.reduce_mean(tf.log(lrelu_deri(x,self.alpha)))

class ZeroFilter(object):
    
    # 3 by 3 filter with weights [0 0 0; 0 0 0; 0 0 0]
    
    def __init__(self,size=3):
        
        self.size = size
        self.filter = tf.constant(np.zeros((size,size,1,1)),dtype=tf.float32)
        
    def get_filter(self):
        return self.filter
    
class ConvLayer(object):
    
    # Layer of C*C filters
    # If order = [1,...,C] the transform for C-->C is lower triangular, 
    # else a permutation of that
    #
    # Possibly nonlinear, the alphas are those of the diagonal filters
    
    def __init__(self,C,layer_id,non_linear=1,use_bias=1,use_constr_filter=0,use_seq_filter=0,seq_filter_size=3):
        
        self.C = C
        self.filters = []
        if use_bias:
            self.bias = tf.Variable(tf.constant(0.01, shape=[C]))
        else:
            self.bias = tf.constant(0.0, shape=[C])
        if C == 1:          
            self.order = [0]           
        if C == 1:
            if layer_id % 2 == 0:
                self.order = [0,1]                
            elif layer_id % 2 == 1:
                self.order = [1,0]            
        if C == 3:
            if layer_id % 6 == 0:
                self.order = [0,1,2]                
            elif layer_id % 6 == 1:
                self.order = [2,0,1] 
            elif layer_id % 6 == 2:
                self.order = [1,2,0]   
            elif layer_id % 6 == 3:
                self.order = [0,2,1] 
            elif layer_id % 6 == 4:
                self.order = [1,0,2]   
            elif layer_id % 6 == 5:
                self.order = [2,1,0]
        else:
            self.order = np.random.permutation(C)
                
        for i in range(C):
            ftmp = []
            for j in range(C):
                if j == i:
                    if use_seq_filter:
                        ftmp.append(SeqFilter(np.random.randint(0,8),seq_filter_size,non_linear))
#                        ftmp.append(SeqFilter(layer_id,f_size,seq_filter_size,non_linear))     
                    elif use_constr_filter:                
                        ftmp.append(ConstrainedPlusFilter(non_linear))        
                    else:
                        ftmp.append(PlusFilter(non_linear))
#                        ftmp.append(KronProdFilter(non_linear))   
                elif j < i:
                    if use_seq_filter:
                        ftmp.append(SeqFilter(np.random.randint(0,8),seq_filter_size,non_linear))
                    elif use_constr_filter:                
                        ftmp.append(ConstrainedPlusFilter(non_linear))    
                    else:
                        ftmp.append(PlusFilter(non_linear)) 
#                    ftmp.append(ScaleFilter(f_size))
                else:
                    ftmp.append(ZeroFilter(f_size))
            self.filters.append(ftmp)
        self.filter = tf.concat([tf.concat([self.filters[i][j].get_filter() for i in self.order],2) for j in self.order],3)

        
    def get_filter(self):
        return self.filter
    
    def get_filters(self):
        return self.filters
    
    def get_bias(self):
        return self.bias
    
    def get_alphas(self):
        return tf.stack([self.filters[j][j].get_alpha() for j in self.order])
    
    def mean_log_det(self,x):
        return tf.reduce_sum([self.filters[j][j].mean_log_det(x[:,:,:,j]) for j in self.order])    

class CNN(object):
    
    def __init__(self,C,L,non_linear=1,use_bias=1,use_constr_filter=0,use_seq_filter=0,seq_filter_size=3):
        
        self.C = C
        self.L = L
        self.layers = []
        for l in range(L):
            ltmp = ConvLayer(C,l,non_linear,use_bias,use_constr_filter,use_seq_filter,seq_filter_size)
            self.layers.append(ltmp)
        
    def forward(self,x):
        z = x
        for l in range(self.L):
            h = tf.nn.conv2d(z, self.layers[l].get_filter(), [1,1,1,1], padding='SAME') + \
                tf.reshape(self.layers[l].get_bias(),[1,1,1,self.C])
            z = lrelu_channelwise(h,self.layers[l].get_alphas(),self.C)
        return z
    
    def forward_and_compute_loss(self,x,image_pad,mask_pad,trends):
        z = x
        mean_log_det = 0
        for l in range(self.L):
            h = tf.nn.conv2d(z, self.layers[l].get_filter(), [1,1,1,1], padding='SAME') + \
                tf.reshape(self.layers[l].get_bias(),[1,1,1,self.C])
            z = lrelu_channelwise(h,self.layers[l].get_alphas(),self.C)
            mean_log_det = mean_log_det + self.layers[l].mean_log_det(h)
        L1 = .5*mean_squared_error_hadamard(image_pad,x+trends,mask_pad)
        L2 = .5*tf.reduce_mean(tf.square(z))
        L3 = -mean_log_det
        return z, L1, L2, L3
        
    def forward_no_bias(self,x):
        z = x
        for l in range(self.L):
            h = tf.nn.conv2d(z, self.layers[l].get_filter(), [1,1,1,1], padding='SAME')
            z = lrelu_channelwise(h,self.layers[l].get_alphas(),self.C)
        return z
    
    def backward_no_bias(self,x): # Make robust to nonlinearites!
        z = x
        for l in range(self.L-1,-1,-1):            
            WT = tf.transpose(tf.reverse(self.layers[l].get_filter(),[0,1]),perm=[0,1,3,2])
            z = tf.nn.conv2d(z, WT, [1,1,1,1], padding='SAME')
        return z
    
    def getWs(self):
        return tf.concat([tf.expand_dims(self.layers[l].get_filter(),-1) for l in range(L)],4)
    
    def get_total_bias(self,sz):
        x0 = tf.zeros([1,sz[0],sz[1],self.C])
        return self.forward(x0)

#%% Define network

L = args.depth
Nq = 10 # Nbr of VI samples per iteration

# Linear trend model
if args.detrend:
    mask_tmp = np.squeeze(mask)==1
    beta_start = np.linalg.lstsq(np.stack([np.ones_like(lon[mask_tmp]),lon[mask_tmp],lat[mask_tmp]],1), \
                                image[mask==1])[0]
    K = len(beta_start)
    trends_start = np.reshape(beta_start[0] + beta_start[1]*lon + beta_start[2]*lat,[sz[0],sz[1],1])
else:
    trends_start = 0.0
    trends = tf.zeros(1)
    trends_mean = trends
    trends_unpad = tf.zeros(1)

mu = tf.Variable(np.reshape(pad_image((image-trends_start)*mask,pad_width),[sz_pad[0], sz_pad[1], C]),dtype=tf.float32) # q mean
s = tf.math.softplus(tf.Variable(tf.random.truncated_normal([sz_pad[0], sz_pad[1], C], stddev=.01))) # q std
mu_unpad = mu[pad_width:pad_width+sz[0],pad_width:pad_width+sz[1],:]

# Variational posterior and samples
q = tfd.MultivariateNormalDiag(loc=mu,scale_diag=s)
x = q.sample(Nq,seed=seed)
if args.detrend:
    v = tf.constant(0.0001,dtype=tf.float32) # beta prior inv sd
    mu_beta = tf.Variable(beta_start,dtype=tf.float32)
    s_beta = tf.math.softplus(tf.Variable(tf.random.truncated_normal([K], stddev=.01)))
    q_beta = tfd.MultivariateNormalDiag(loc=mu_beta,scale_diag=s_beta)
    beta = q_beta.sample(Nq,seed=seed)
    lon_pad_tf = tf.reshape(tf.constant(lon_pad,dtype=tf.float32),[1,sz_pad[0],sz_pad[1],1])
    lat_pad_tf = tf.reshape(tf.constant(lat_pad,dtype=tf.float32),[1,sz_pad[0],sz_pad[1],1])
    trends = tf.reshape(beta[:,0],[Nq,1,1,1]) + tf.reshape(beta[:,1],[Nq,1,1,1])*tf.tile(lon_pad_tf,[Nq,1,1,1]) + \
             tf.reshape(beta[:,2],[Nq,1,1,1])*tf.tile(lat_pad_tf,[Nq,1,1,1]) 
    trends_mean = mu_beta[0] + mu_beta[1]*lon_pad_tf + mu_beta[2]*lat_pad_tf
    trends_unpad = tf.reshape(trends_mean[:,pad_width:pad_width+sz[0],pad_width:pad_width+sz[1],:],[sz[0],sz[1],1])

if args.learn_sigmasq:
    sigmaSq = tf.exp(tf.Variable(tf.log(0.000001),dtype=tf.float32))    
else:
    sigmaSq = tf.constant(0.000001,dtype=tf.float32)

# Define network
cnn = CNN(C,L,args.non_linear,args.layer_bias,args.use_constr_filter,args.use_seq_filter,args.seq_filter_size)
z, L1, L2, L3 = cnn.forward_and_compute_loss(x,image_pad,mask_pad,trends)

L4 = -tf.reduce_mean(tf.log(s))
L5 = .5*(M/N)*tf.log(sigmaSq)
if args.detrend:
    L6 = 1/N*(-K*tf.log(v) + 0.5*tf.square(v)*(tf.reduce_sum(tf.square(s_beta))+tf.reduce_sum(tf.square(mu_beta))))
    L5 = L5 + L6

Ltot = (1/sigmaSq)*L1 + L2 + L3 + L4 + L5
Lval = .5*mean_squared_error_hadamard(image_pad,x+trends,val_mask_pad)

optimizer = tf.train.AdamOptimizer(learning_rate=args.learning_rate)
update_ops = tf.get_collection(tf.GraphKeys.UPDATE_OPS)
with tf.control_dependencies(update_ops):
    train_op = optimizer.minimize(Ltot)
    
sess = tf.InteractiveSession()
sess.run(tf.global_variables_initializer())
saver = tf.train.Saver()
train_vars = tf.trainable_variables()
Ws = cnn.getWs()
W = Ws[:,:,:,:,0]

#%% Possibly load weights
start_step = 0
if args.restore_from is not None:
    if args.restore_from == 'latest':
        filelist1 = os.listdir(args.logdir)
        filelist2 = [f for f in filelist1 if f[0:5]=='model' and f[-4:]=='meta']
        iter_nbrs = [int(f.split('model-step-')[1].split('-lt')[0]) for f in filelist2]
        restore_from = filelist2[np.argmax(iter_nbrs)][:-5]
    else:
        restore_from = args.restore_from
    saver.restore(sess, args.logdir + '/' + restore_from)
    start_step = int(restore_from.split('step-')[1].split('-')[0])
    print('Model restored from ' + args.logdir + '/' + restore_from)

min_loss = 100000.0

#%% Training loop
lt_vec = []
lv_vec = []

if args.do_optim:
    for i in range(start_step, start_step + args.num_steps+1):
        _, lt  = sess.run([train_op, Ltot])
        if i % args.checkpoint_every == 0:
            
            if args.do_plot:
                mu_unpad_out, trends_unpad_out = sess.run([mu_unpad,trends_unpad])                
                image_out = mu_unpad_out + trends_unpad_out
                fig,ax = plt.subplots(1)
                im = ax.imshow(np.squeeze(np.concatenate((image,image_out),axis=1)))
                if not "mask" in mat:   
                    plot_toy_mask(sz,ax,0,0,linewidth=0.3,linestyle="--",edgecolor='black',facecolor='none')
                    plot_toy_mask(sz,ax,0,1,linewidth=0.3,linestyle="--",edgecolor='black',facecolor='none')
                fig.colorbar(im)
                ax.set_title('Data and VI posterior mean')
                plt.show()
            lt1, lt2, lt3, lt4, lv  = sess.run([L1, L2, L3, L4, Lval])            
            print("Optimizing",args.dataset_nbr,":",datetime.now().strftime('%Y-%m-%d %H:%M:%S'),i, lt, lt1, lt2, lt3, lt4, lv)
            lt_vec.append(lt)
            lv_vec.append(lv)
            
            if not os.path.exists(args.logdir):
                os.makedirs(args.logdir)
                checkpoint_path = os.path.join(args.logdir, "model-step-%d-lt-%g-lv-%g.ckpt" % (i, lt, lv))
                filename = saver.save(sess, checkpoint_path)
                print("Model saved in file: %s" % filename)
            elif lt < min_loss:
                min_loss = lt
                if not os.path.exists(args.logdir):
                    os.makedirs(args.logdir)
                checkpoint_path = os.path.join(args.logdir, "model-step-%d-lt-%g-lv-%g.ckpt" % (i, lt, lv))
                filename = saver.save(sess, checkpoint_path)
                print("Model saved in file: %s" % filename)
                 
    print('Optimization done.')
    if not args.debug:
        saver.restore(sess,filename)   
        print('Model restored from ' + filename)
        
z_out, W_out, mu_unpad_out, mu_out, s_out, sigmaSq_out, trends_out, trends_unpad_out = \
                    sess.run([z, W, mu_unpad, mu, s, sigmaSq, trends_mean, trends_unpad])
                
image_out = mu_unpad_out + trends_unpad_out

#%% Save VI results for non-linear case
sd_unpad = s_out[pad_width:pad_width+sz[0],pad_width:pad_width+sz[1],:]

if args.non_linear:
    spio.savemat('data/outdata/results' + str(args.dataset_nbr) + str(args.method_name) + '.mat', \
                 {"mean":np.squeeze(image_out),"sd":np.squeeze(sd_unpad)})

#%% Plot optimization results

#Plot image and vi mean
fig,ax = plt.subplots(1)
im = ax.imshow(np.squeeze(np.concatenate((image,image_out),axis=1)))
if not "mask" in mat:       
    plot_toy_mask(sz,ax,0,0,linewidth=0.3,linestyle="--",edgecolor='black',facecolor='none')
    plot_toy_mask(sz,ax,0,1,linewidth=0.3,linestyle="--",edgecolor='black',facecolor='none')
fig.colorbar(im)
ax.set_title('Data and VI posterior mean') 
if args.do_plot:
    plt.show()
else:
    plt.savefig('data/outdata/output' + str(args.dataset_nbr) + str(args.method_name) + '.png',bbox_inches='tight',dpi=500)
plt.clf()

# Plot/print filter1
Wplot = np.squeeze(W_out[:,:,0,0])
if args.do_plot:
    plt.imshow(Wplot)
    plt.colorbar()
    plt.title("One filter")
    plt.show()
print(Wplot)

# Plot mu
fig,ax = plt.subplots(1)
im = ax.imshow(np.squeeze(image_out))
if not "mask" in mat:         
    plot_toy_mask(sz,ax,0,0,linewidth=0.3,linestyle="--",edgecolor='black',facecolor='none')
fig.colorbar(im)
ax.set_title('VI posterior mean') 
if args.do_plot:
    plt.show()
else:
    plt.savefig('data/outdata/vi_mean' + str(args.dataset_nbr) + str(args.method_name) + '.png',bbox_inches='tight',dpi=500)
plt.clf()
    
# Plot s
s_out_row = np.concatenate([s_out[pad_width:pad_width+sz[0],pad_width:pad_width+sz[1],i] for i in range(C)],1)
fig,ax = plt.subplots(1)
im = ax.imshow(s_out_row)
if not "mask" in mat:      
    plot_toy_mask(sz,ax,0,0,linewidth=0.3,linestyle="--",edgecolor='black',facecolor='none')
fig.colorbar(im)
ax.set_title('VI posterior sd') 
if args.do_plot:
    plt.show()
else:
    plt.savefig('data/outdata/vi_sd' + str(args.dataset_nbr) + str(args.method_name) + '.png',bbox_inches='tight',dpi=500)
plt.clf()  

# Plot loss over iterations
start_id = 200
fig,axs = plt.subplots(2,1,sharex=True)
axs[0].plot(args.checkpoint_every*np.arange(len(lt_vec))[start_id:],lt_vec[start_id:])
axs[0].set_title('Train loss')
axs[1].plot(args.checkpoint_every*np.arange(len(lv_vec))[start_id:],lv_vec[start_id:])
axs[1].set_title('Validation loss')
axs[1].set_xlabel('Iterations')    
if args.do_plot:
    plt.show()
else:
    plt.savefig('data/outdata/loss_curve' + str(args.dataset_nbr) + str(args.method_name) + '.eps',bbox_inches='tight',dpi=500)
plt.clf()   
spio.savemat('data/outdata/loss_curve' + str(args.dataset_nbr) + str(args.method_name) + '.mat', {"lt":lt_vec,"lv":lv_vec})


#%% Functions for sampling
    
mask_pad2 = tf.reshape(mask_pad,shape=[1,sz_pad[0],sz_pad[1],C])
if args.detrend and C==1:  
    lon_ext = tf.tile(tf.reshape(tf.constant(lon_pad,dtype=tf.float32),[sz_pad[0],sz_pad[1],1,1]),[1,1,1,1])
    lat_ext = tf.tile(tf.reshape(tf.constant(lat_pad,dtype=tf.float32),[sz_pad[0],sz_pad[1],1,1]),[1,1,1,1])
    F = tf.concat([tf.ones_like(lon_ext),lon_ext,lat_ext],axis=3)
    mask_pad3 = tf.reshape(mask_pad2,[sz_pad[0],sz_pad[1],1,1])
    ImF = tf.multiply(mask_pad3,F)
    FTImF = tf.tensordot(F,ImF,[[0,1,2],[0,1,2]])

def GTG(x):
    z = cnn.forward_no_bias(x)
    z = cnn.backward_no_bias(z)
    return z

def QTilde(x):
    z = cnn.forward_no_bias(x)
    z = cnn.backward_no_bias(z) + (1/sigmaSq)*mask_pad2*x
    return z    

def QTilde_detrend(x):
    z0 = QTilde(x[0]) + (1/sigmaSq)*tf.tensordot(x[1],ImF,[[1],[3]])
    z1 = tf.tensordot((1/sigmaSq)*x[0],ImF,axes=[[1,2,3],[0,1,2]]) + \
          tf.square(v)*x[1] + (1/sigmaSq)*tf.tensordot(x[1],FTImF,[[1],[0]])
    return [z0,z1]

def cg(A,b,max_iter):
    
    temp = set(tf.all_variables())
    
    b_shape = b.get_shape().as_list()
    Ns = b_shape[0]
    sz_pad = b_shape[1:3]
    C = b_shape[3]
    
    x_s = tf.Variable(tf.zeros([Ns,sz_pad[0], sz_pad[1], C]),name="x_s")
    z = A(x_s)
    
    # Do Conjugate Gradients
    n2b = tf.sqrt(tf.reduce_sum(tf.square(b),[1,2,3]))
    r = b - z
        
    # The first iteration in Matlab version loop is here done before the loop
    rho = tf.reshape(tf.reduce_sum(tf.multiply(r,r),[1,2,3]),(Ns,1))
    p = r
    q = A(p)
    pq = tf.reshape(tf.reduce_sum(tf.multiply(p,q),[1,2,3]),(Ns,1))
    alpha = tf.divide(tf.reshape(tf.reduce_sum(tf.multiply(r,r),[1,2,3]),(Ns,1)),pq)
    
    # Initialize assign variables
    x_s = tf.Variable(x_s + tf.multiply(tf.reshape(alpha,[Ns,1,1,1]),p))
    r = tf.Variable(r - tf.multiply(tf.reshape(alpha,[Ns,1,1,1]),q))
    rho1 = tf.Variable(rho)
    p1 = tf.Variable(p)
        
    # Loop
    rho = tf.reshape(tf.reduce_sum(tf.multiply(r,r),[1,2,3]),(Ns,1))
    beta = tf.divide(rho,rho1)
    p = r + tf.multiply(tf.reshape(beta,[Ns,1,1,1]),p1)
    q = A(p)
    pq = tf.reshape(tf.reduce_sum(tf.multiply(p,q),[1,2,3]),(Ns,1))
    alpha = tf.divide(rho,pq)
    
    with tf.control_dependencies([rho,p]):
        assign_x_s = tf.assign(x_s,x_s + tf.multiply(tf.reshape(alpha,[Ns,1,1,1]),p))
        assign_r = tf.assign(r,r - tf.multiply(tf.reshape(alpha,[Ns,1,1,1]),q))
        with tf.control_dependencies([assign_x_s,assign_r]):
            assign_rho1 = tf.assign(rho1,rho)
            assign_p1 = tf.assign(p1,p)    
    
    normr = tf.sqrt(tf.reduce_sum(tf.square(r),[1,2,3]))
    relres = normr / n2b
    
    sess.run(tf.initialize_variables(set(tf.all_variables()) - temp))
        
    # CG loop
    for i in range(max_iter):
        sess.run([assign_x_s,assign_r,assign_rho1,assign_p1])
        
        if i % args.checkpoint_every == 0:            
            tmp = sess.run(relres)   
            print("Conjugate gradient",args.dataset_nbr,":",datetime.now().strftime('%Y-%m-%d %H:%M:%S'),i,tmp)
            if np.max(np.log10(tmp)) < args.cg_tol_log10 and i >= args.cg_min_iter:
                break
        if i == max_iter - 1:
            print("WARNING: Conjugate gradient did not reach below the set tolerance:",datetime.now().strftime('%Y-%m-%d %H:%M:%S'))
    return x_s, sess

def cg_detrend(A,b,max_iter):
    
    temp = set(tf.all_variables())
    
    b_shape = b[0].get_shape().as_list()
    Ns = b_shape[0]
    sz_pad = b_shape[1:3]
    C = b_shape[3]
    K = b[1].get_shape().as_list()[1]
    
    x_s_a = tf.Variable(tf.zeros([Ns,sz_pad[0], sz_pad[1], C]))
    x_s_b = tf.Variable(tf.zeros([Ns,K]))
    z = A([x_s_a,x_s_b])
    
    # Do Conjugate Gradients
    n2b = tf.sqrt(tf.reduce_sum(tf.square(b[0]),[1,2,3])+tf.reduce_sum(tf.square(b[1]),[1]))
    r_a = b[0] - z[0]
    r_b = b[1] - z[1]
        
    # The first iteration in Matlab version loop is here done before the loop
    rho = tf.reshape(tf.reduce_sum(tf.multiply(r_a,r_a),[1,2,3])+tf.reduce_sum(tf.multiply(r_b,r_b),[1]),(Ns,1))
    p_a = r_a
    p_b = r_b
    q = A([p_a,p_b])
    pq = tf.reshape(tf.reduce_sum(tf.multiply(p_a,q[0]),[1,2,3])+tf.reduce_sum(tf.multiply(p_b,q[1]),[1]),(Ns,1))
    alpha = tf.divide(tf.reshape(tf.reduce_sum(tf.multiply(r_a,r_a),[1,2,3])+tf.reduce_sum(tf.multiply(r_b,r_b),[1]),(Ns,1)),pq)
    
    # Initialize assign variables
    x_s_a = tf.Variable(x_s_a + tf.multiply(tf.reshape(alpha,[Ns,1,1,1]),p_a))
    x_s_b = tf.Variable(x_s_b + tf.multiply(tf.reshape(alpha,[Ns,1]),p_b))
    r_a = tf.Variable(r_a - tf.multiply(tf.reshape(alpha,[Ns,1,1,1]),q[0]))
    r_b = tf.Variable(r_b - tf.multiply(tf.reshape(alpha,[Ns,1]),q[1]))
    rho1 = tf.Variable(rho)
    p1_a = tf.Variable(p_a)
    p1_b = tf.Variable(p_b)
    
    # Loop
    rho = tf.reshape(tf.reduce_sum(tf.multiply(r_a,r_a),[1,2,3])+tf.reduce_sum(tf.multiply(r_b,r_b),[1]),(Ns,1))
    beta = tf.divide(rho,rho1)
    p_a = r_a + tf.multiply(tf.reshape(beta,[Ns,1,1,1]),p1_a)
    p_b = r_b + tf.multiply(tf.reshape(beta,[Ns,1]),p1_b)
    q = A([p_a,p_b])
    pq = tf.reshape(tf.reduce_sum(tf.multiply(p_a,q[0]),[1,2,3])+tf.reduce_sum(tf.multiply(p_b,q[1]),[1]),(Ns,1))
    alpha = tf.divide(rho,pq)
    
    with tf.control_dependencies([rho,p_a,p_b]):
        assign_x_s_a = tf.assign(x_s_a,x_s_a + tf.multiply(tf.reshape(alpha,[Ns,1,1,1]),p_a))
        assign_x_s_b = tf.assign(x_s_b,x_s_b + tf.multiply(tf.reshape(alpha,[Ns,1]),p_b))
        assign_r_a = tf.assign(r_a,r_a - tf.multiply(tf.reshape(alpha,[Ns,1,1,1]),q[0]))
        assign_r_b = tf.assign(r_b,r_b - tf.multiply(tf.reshape(alpha,[Ns,1]),q[1]))
        with tf.control_dependencies([assign_x_s_a,assign_x_s_b,assign_r_a,assign_r_b]):
            assign_rho1 = tf.assign(rho1,rho)
            assign_p1_a = tf.assign(p1_a,p_a)   
            assign_p1_b = tf.assign(p1_b,p_b)    
    
    normr = tf.sqrt(tf.reduce_sum(tf.square(r_a),[1,2,3])+tf.reduce_sum(tf.square(r_b),[1]))
    relres = normr / n2b
    
    sess.run(tf.initialize_variables(set(tf.all_variables()) - temp))
        
    # CG loop
    for i in range(max_iter):
        sess.run([assign_x_s_a,assign_x_s_b,assign_r_a,assign_r_b,assign_rho1,assign_p1_a,assign_p1_b])
        
        if i % args.checkpoint_every == 0:            
            tmp = sess.run(relres)   
            print("Conjugate gradient",args.dataset_nbr,":",datetime.now().strftime('%Y-%m-%d %H:%M:%S'),i,tmp)
            if np.max(np.log10(tmp)) < args.cg_tol_log10 and i >= args.cg_min_iter:
                break
        if i == max_iter - 1:
            print("WARNING: Conjugate gradient did not reach below the set tolerance:",datetime.now().strftime('%Y-%m-%d %H:%M:%S'))
    return x_s_a, x_s_b, sess

#%% Posterior mean and sampling    
if args.debug:
    Ns = 2
else:
    Ns = args.nbr_posterior_samples

if Ns > 0 and args.non_linear == 0:
    
    total_bias = cnn.get_total_bias(sz_pad)
    u_s1_tmp = np.concatenate([np.zeros(shape=[1,sz_pad[0],sz_pad[1],C]),np.random.normal(size=[Ns,sz_pad[0],sz_pad[1],C])],axis=0)
    u_s1 = tf.constant(u_s1_tmp,dtype=tf.float32) # u_s1 for posterior mean and samples        
    rhs1 = cnn.backward_no_bias(u_s1-total_bias)
    if args.detrend:
        u_s1_beta_tmp = np.concatenate([np.zeros(shape=[1,K]),np.random.normal(size=[Ns,K])],axis=0)
        u_s1_beta = tf.constant(u_s1_beta_tmp,dtype=tf.float32)
        rhs1_beta = v*u_s1_beta    
    
    u_s2_tmp1 = np.zeros([1,sz_pad[0],sz_pad[1],C])
    u_s2_tmp2 = np.concatenate([np.reshape(pad_image(mask*np.random.normal(size=[sz[0],sz[1],C]),pad_width),[1,sz_pad[0],sz_pad[1],C]) for i in range(Ns)],0)
    u_s2 = tf.constant(np.concatenate([u_s2_tmp1,u_s2_tmp2],axis = 0),dtype=tf.float32)        
    if not args.detrend:
        y = tf.reshape(tf.constant(pad_image(mask*(image-trends_unpad_out),pad_width),dtype=tf.float32), \
                        shape=[1,sz_pad[0],sz_pad[1],C])
    else:        
        y = tf.reshape(tf.constant(pad_image(mask*image,pad_width),dtype=tf.float32), \
                        shape=[1,sz_pad[0],sz_pad[1],C])
    rhs2 = (1/sigmaSq)*(tf.tile(y,[1+Ns,1,1,1])+tf.sqrt(sigmaSq)*u_s2)
    rhs = rhs1 + rhs2
    if args.detrend:
        rhs2_beta = tf.tensordot(rhs2,F,axes=[[1,2,3],[0,1,2]])
        rhs_beta = rhs1_beta + rhs2_beta
        
    if not args.detrend:
        if args.debug:
            x_s, cg_sess = cg(QTilde,rhs,101)
        else:            
            x_s, cg_sess = cg(QTilde,rhs,args.cg_max_iter+1)    
        x_s_out = cg_sess.run(x_s)
        post_mean = x_s_out[0,:,:,:] + trends_out
        post_samples = x_s_out[1:,:,:,:] + trends_out
    else:
        if args.debug:
            x_s_a, x_s_b, cg_sess = cg_detrend(QTilde_detrend,[rhs,rhs_beta],101)
        else:
            x_s_a, x_s_b, cg_sess = cg_detrend(QTilde_detrend,[rhs,rhs_beta],args.cg_max_iter+1)     
        trends_cg = tf.tensordot(x_s_b,F,[[1],[3]])
        x_s_a_out, x_s_b_out, trends_out = cg_sess.run([x_s_a,x_s_b,trends_cg])
        post_mean_a = x_s_a_out[0,:,:,:]
        post_samples_a = x_s_a_out[1:,:,:,:]
        post_mean_b = x_s_b_out[0,:]
        post_samples_b = x_s_b_out[1:,:]
        post_mean = post_mean_a + trends_out[0,:,:,:]
        post_samples = post_samples_a + trends_out[1:,:,:,:]
        
        print(post_mean_b)
        print(np.std(post_samples_b,0))

    # Plot posterior mean 
#    post_mean_plot = np.concatenate([post_mean[pad_width:pad_width+sz[0],pad_width:pad_width+sz[1],i] for i in range(C)],1)
    post_mean_plot = np.squeeze(post_mean)[pad_width:pad_width+sz[0],pad_width:pad_width+sz[1]]
    fig,ax = plt.subplots(1)
    im = ax.imshow(post_mean_plot)
    if not "mask" in mat:           
        plot_toy_mask(sz,ax,0,0,linewidth=0.3,linestyle="--",edgecolor='black',facecolor='none')
    fig.colorbar(im)
    ax.set_title('Posterior mean') 
    if args.do_plot:
        plt.show()
    else:
        plt.savefig('data/outdata/post_mean' + str(args.dataset_nbr) + str(args.method_name) + '.png',bbox_inches='tight',dpi=500)
    plt.clf()
    
    # Plot posterior sample
    post_samples_plot = np.concatenate([post_samples[i,pad_width:pad_width+sz[0],pad_width:pad_width+sz[1],:] for i in range(min([Ns,2]))],1)
    #    post_samples_plot = np.concatenate([post_samples[0,pad_width:pad_width+sz[0],pad_width:pad_width+sz[1],i] for i in range(C)],1)
    fig,ax = plt.subplots(1)
    im = ax.imshow(np.squeeze(post_samples_plot))
    if not "mask" in mat:        
        plot_toy_mask(sz,ax,0,0,linewidth=0.3,linestyle="--",edgecolor='black',facecolor='none')
        plot_toy_mask(sz,ax,0,1,linewidth=0.3,linestyle="--",edgecolor='black',facecolor='none')
    fig.colorbar(im)
    ax.set_title('Posterior sample') 
    if args.do_plot:
        plt.show()
    else:
        plt.savefig('data/outdata/post_samples' + str(args.dataset_nbr) + str(args.method_name) + '.png',bbox_inches='tight',dpi=500)
    plt.clf()
    
if Ns > 1 and args.non_linear == 0:
            
    # MC sd estimate
    post_sd_mc = np.reshape(np.std(post_samples,0),[sz_pad[0],sz_pad[1],C])[pad_width:pad_width+sz[0],pad_width:pad_width+sz[1]]
        
    post_sd = np.concatenate([post_sd_mc[:,:,i] for i in range(C)],1)
    fig,ax = plt.subplots(1)
    im = ax.imshow(post_sd)
    if not "mask" in mat: 
        plot_toy_mask(sz,ax,0,0,linewidth=0.3,linestyle="--",edgecolor='black',facecolor='none')
    fig.colorbar(im) 
    ax.set_title('Posterior sd') 
    if args.do_plot:
        plt.show()
    else:
        plt.savefig('data/outdata/post_sd' + str(args.dataset_nbr) + str(args.method_name) + '.png',bbox_inches='tight',dpi=500)
    plt.clf()
    
    if C == 1 and not args.detrend:
            
        # RBMC sd estimate 
        tmp_shape = (1,21,21,C)
        idx = tf.equal(tf.range(tmp_shape[1]*tmp_shape[2]),tmp_shape[2]*tmp_shape[1]//2)
        x_tmp = tf.where(tf.reshape(idx, tmp_shape),tf.ones(tmp_shape, dtype=tf.float32),tf.zeros(tmp_shape, dtype=tf.float32))
        x_tmp2 = GTG(x_tmp)
        GTGdiag = x_tmp2[0,tmp_shape[1]//2,tmp_shape[2]//2,0]
        diagQ = GTGdiag + (1/sigmaSq)*mask_pad2
        demeaned_samples = post_samples-post_mean
        sd_tmp = tf.sqrt(1/diagQ + 1/Ns*tf.reduce_sum(tf.square((QTilde(demeaned_samples)-diagQ*demeaned_samples)/diagQ),0))
        post_sd_rbmc = tf.reshape(sd_tmp,sz_pad)[pad_width:pad_width+sz[0],pad_width:pad_width+sz[1]]
        post_sd_rbmc_out = sess.run(post_sd_rbmc)
        
        post_sd = post_sd_rbmc_out
        fig,ax = plt.subplots(1)
        im = ax.imshow(post_sd)
        if not "mask" in mat:
            plot_toy_mask(sz,ax,0,0,linewidth=0.3,linestyle="--",edgecolor='black',facecolor='none')
        fig.colorbar(im) 
        ax.set_title('Posterior sd rbmc') 
        if args.do_plot:
            plt.show()
        else:
            plt.savefig('data/outdata/post_sd_rbmc' + str(args.dataset_nbr) + str(args.method_name) + '.png',bbox_inches='tight',dpi=500)
        plt.clf()

    # Save mean and sd
    if not args.do_plot:     
        spio.savemat('data/outdata/results' + str(args.dataset_nbr) + str(args.method_name) + '.mat',\
                      {"mean":post_mean_plot,"sd":post_sd,"sigmaSq":sigmaSq_out,"samples":post_samples_plot})   
            
#%% Plot total linear filter, C=1 case
import scipy.signal as spsi
if C == 1:
    Ws_ = sess.run(Ws)
    f_size = Ws_.shape[0]
    filters = [np.reshape(Ws_[:,:,0,0,0],[f_size,f_size])]
    forward_filter = np.reshape(Ws_[:,:,0,0,0],[f_size,f_size])
    for i in range(1,Ws_.shape[4]):
        filters.append(np.reshape(Ws_[:,:,0,0,i],[f_size,f_size]))
        forward_filter = spsi.convolve2d(np.reshape(Ws_[:,:,0,0,i],[f_size,f_size]),forward_filter)
    precmat_stencil = spsi.convolve2d(np.flip(forward_filter,[0,1]),forward_filter)
    if args.do_plot:
        plt.imshow(forward_filter)
        plt.colorbar()
        plt.title('Forward filter')
        plt.show()
        
        plt.imshow(precmat_stencil)
        plt.colorbar()
        plt.title('Precision matrix stencil')
        plt.show()
        
    resulting_size = (f_size-1)*L+1
    
    all_filters = np.concatenate([f for f in filters],1)
    fig,ax = plt.subplots(1)
    im = ax.imshow(all_filters)
    fig.colorbar(im,fraction=0.0214, pad=0.02, aspect = 15)
    
    ax.set_title(' filter 1              filter 2             filter 3',pad=4,fontsize=14)
    plt.axvline(x=3-.5,color='black',linewidth=0.5)
    plt.axvline(x=6-.5,color='black',linewidth=0.5)
    ax.set_xticks([])
    ax.set_yticks([])
    ax.set_yticklabels([])
    ax.set_xticklabels([])
    if args.do_plot:
        plt.show()
    elif True:
        plt.savefig('data/outdata/paper_fig2b' + str(args.dataset_nbr) + str(args.method_name) + '.png',bbox_inches='tight',dpi=1000)
    plt.clf()

#%% Plot intermediate layer image, C=1 case
if C == 1:
    images = [np.squeeze(image-trends_unpad_out)]
    # images = [np.squeeze(mu_out)]
    for i in range(Ws_.shape[4]):
        images.append(spsi.convolve2d(images[-1],np.reshape(Ws_[:,:,0,0,i],[f_size,f_size]),mode='valid'))
    all_layers = np.concatenate([np.pad(images[i],i*(f_size//2)) for i in range(Ws_.shape[4]+1)],1)

    fig,ax = plt.subplots(1)
    im = ax.imshow(all_layers)
    cbar = fig.colorbar(im,fraction=0.0214, pad=0.015, aspect = 15)#, ticks = [-20,-10,0,10,20])
    cbar.ax.tick_params(labelsize=8)
    
    title_tmp = '    '.join(['    data        ','      layer 1    ','      layer 2     ','     layer 3  '])
    ax.set_title(title_tmp,pad=3,fontsize=10)
    plt.axvline(x=120-.5,color='black',linewidth=0.5)
    plt.axvline(x=240-.5,color='black',linewidth=0.5)
    plt.axvline(x=360-.5,color='black',linewidth=0.5)
    # plt.axis('off')
    ax.set_xticks([])
    ax.set_yticks([])
    ax.set_yticklabels([])
    ax.set_xticklabels([])
    if args.do_plot:
        plt.show()
    elif True:
        plt.savefig('data/outdata/paper_fig2a' + str(args.dataset_nbr) + str(args.method_name) + '.png',bbox_inches='tight',dpi=1200)
    plt.clf()

