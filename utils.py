#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Mon Nov 25 14:27:34 2019

Utility functions and classes

"""

#%% Import
import tensorflow as tf
import numpy as np
import matplotlib.patches as patches

#%% Functions
def mean_squared_error_hadamard(y_true, y_pred, mask):
    Ns = y_pred.get_shape().as_list()[0]
    return tf.divide(tf.reduce_sum(tf.square(mask * (y_true - y_pred))),Ns*tf.reduce_sum(mask))

def lrelu(x,alpha):
    return x - tf.to_float(x<0)*(1.0-alpha)*x

def lrelu_deri(x,alpha):
    deri = tf.ones_like(x)
    deri = deri - tf.to_float(x<0)*(1.0-alpha)
    return deri

def lrelu_channelwise(x,alphas,C):
    return x - tf.to_float(x<0)*(1.0-tf.reshape(alphas,shape=[1,1,1,C]))*x

def lrelu_deri_channelwise(x,alphas,C):
    deri = tf.ones_like(x)
    deri = deri - tf.to_float(x<0)*(1.0-tf.reshape(alphas,shape=[1,1,1,C]))
    return deri

def get_toy_mask(sz):
    mask = np.ones(sz)
    mask[int(.4*sz[0]):int(.6*sz[0]),int(.3*sz[1]):int(.7*sz[1])] = 0
    mask[int(.7*sz[0]):int(.9*sz[0]),int(.3*sz[1]):int(.3*sz[1])+8] = 0
    mask[int(.7*sz[0]):int(.9*sz[0]),int(.5*sz[1]):int(.5*sz[1])+4] = 0
    mask[int(.7*sz[0]):int(.9*sz[0]),int(.7*sz[1])-1:int(.7*sz[1])] = 0
    mask[int(.1*sz[0]):int(.1*sz[0]+8),int(.3*sz[1]):int(.7*sz[1])] = 0
    mask[int(.2*sz[0]):int(.2*sz[0]+4),int(.3*sz[1]):int(.7*sz[1])] = 0
    mask[int(.3*sz[0]):int(.3*sz[0]+1),int(.3*sz[1]):int(.7*sz[1])] = 0
    return mask

def plot_toy_mask(sz,ax,i,j,**kwargs):
    ax.add_patch(patches.Rectangle((int((j+.3)*sz[1])-.5,int((i+.4)*sz[0])-.5),int(.4*sz[1]),int(.2*sz[0]),**kwargs))
    ax.add_patch(patches.Rectangle((int((j+.3)*sz[1])-.5,int((i+.7)*sz[0])-.5),8,int(.2*sz[0]),**kwargs))
    ax.add_patch(patches.Rectangle((int((j+.5)*sz[1])-.5,int((i+.7)*sz[0])-.5),4,int(.2*sz[0]),**kwargs))
    ax.add_patch(patches.Rectangle((int((j+.7)*sz[1])-1.5,int((i+.7)*sz[0])-.5),1,int(.2*sz[0]),**kwargs))
    ax.add_patch(patches.Rectangle((int((j+.3)*sz[1])-.5,int((i+.1)*sz[0])-.5),int(.4*sz[1]),8,**kwargs))
    ax.add_patch(patches.Rectangle((int((j+.3)*sz[1])-.5,int((i+.2)*sz[0])-.5),int(.4*sz[1]),4,**kwargs))
    ax.add_patch(patches.Rectangle((int((j+.3)*sz[1])-.5,int((i+.3)*sz[0])-.5),int(.4*sz[1]),1,**kwargs))

def pad_image(im,pad_width):
    npad = ((pad_width, pad_width), (pad_width, pad_width), (0, 0))
    im_pad = np.pad(im,pad_width=npad)
    return im_pad

def pad_lon(lon,pad_width):
    first = lon[0,0]
    last = lon[-1,0]
    deriv = (last - first) / (lon.shape[0]-1)
    new_first = first - pad_width*deriv
    new_last = last + pad_width*deriv
    lon_pad = np.transpose(np.tile(np.linspace(new_first,new_last,lon.shape[0]+2*pad_width),(lon.shape[1]+2*pad_width,1)))
    return lon_pad

def pad_lat(lat,pad_width):
    return np.transpose(pad_lon(np.transpose(lat),pad_width))
